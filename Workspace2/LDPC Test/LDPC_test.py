import numpy as np
import matplotlib.pyplot as plt
import pyISCML.ldpc as ldpc

#rate 0.5 größte blocksize
H_rows, H_cols, P = ldpc.InitializeWiMaxLDPC(3/4,576)
bits = np.random.choice([0,1], int(576*0.75)).astype("int32")
print("bits")
print(bits)
print(bits.shape)
encoded = ldpc.Encode(bits,H_rows, P)
print("encoded")
print(encoded)
print(encoded.shape)
llr = 1 - 2*encoded
[output, errors] = ldpc.MpDecode(llr, H_rows, H_cols, 20, 1)

print(output[19])
print(output[19].shape)
print(output[19,:len(bits)]-bits)
print(errors)