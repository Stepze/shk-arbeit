# -*- coding: utf-8 -*-

import numpy as np
import scipy as sp


#ENCODEDBITS Encodes incoming bitstream with a given trellis structure.
#   Please note that the trellis structure needs to be like the one in the 
#   example in order to decode trellis structures that do not satisfy the 
#   Example:
#   conditions of convolutional codes.
#
#     trellis = struct('numInputSymbols',2,'numOutputSymbols',4,...
#     'numStates',4,'nextStates',[2 3;0 2;0 1;2 3],...
#     'outputs',["00" "00";"01" "00";"01" "10";"00" "00"],...
#     'fromStates',[1 2 inf inf;2 inf inf inf;0 1 3 inf; 0 3 inf inf]);
#
#   Please note that there is an additional field: fromStates and that the outputs
#   are given as strings

def rllEncoderFct(inputBits, trellis):
	assert type(trellis) == dict

	currentState = 0
	encodedBits = np.zeros((2*len(inputBits)));
	#Construct output in doubles
	trellisOutputsDouble = np.zeros((2,trellis["numStates"],trellis["numInputSymbols"])).astype("double")
	for curInputSym in range(trellis["numInputSymbols"]):
		for curState in range(trellis["numStates"]):
			tmp = str(trellis["outputs"][curState,curInputSym])
			for curOutputBit in range(int(np.log2(trellis["numOutputSymbols"]))):
				trellisOutputsDouble[curOutputBit,curState,curInputSym] = float(tmp[curOutputBit])

	#Encode
	for i in range(len(inputBits)):
		nextState = trellis["nextStates"][currentState,inputBits[i]]
		encodedBits[i*2:(i+1)*2] = trellisOutputsDouble[:,currentState,inputBits[i]]
		currentState = nextState

	return encodedBits


def nrziEncodingFct(inputBits):
	# 1--> level Change
	# 0--> no level change

	#First Generate Correct Size Matrix
	codedBits=np.zeros((inputBits.shape[0]))

	#initialize First Bit with level -1
	codedBits[0] = -1

	#Differential Coding:
	for i in range(1,codedBits.shape[0]):
			if inputBits[i] == 1:
				codedBits[i]= -codedBits[i-1]
			else:
				codedBits[i] = codedBits[i-1]
	return codedBits