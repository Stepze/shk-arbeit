# -*- coding: utf-8 -*-
import numpy as np

class RLL_BCJR:
	def __init__(self, rllD):
		#works only with a d constraint of d=2
		assert rllD == 2

		#Hard coding for RLL code with R=1/2
		self.k = 1
		self.n = 2
		self.numStates = 8
		
		#Define code
		#1 indicates '-1' and 2 indicates '+1'
		self.codeDef = np.zeros((self.n,self.numStates,self.numStates))
		self.codeDef[:,2,0] = np.array([1,1])
		self.codeDef[:,3,0] = np.array([1,1])
		self.codeDef[:,4,1] = np.array([1,2])
		self.codeDef[:,2,1] = np.array([1,1])
		self.codeDef[:,4,2] = np.array([1,2])
		self.codeDef[:,5,2] = np.array([2,2])
		self.codeDef[:,2,3] = np.array([1,1])
		self.codeDef[:,3,3] = np.array([1,1])
		self.codeDef[:,6,4] = np.array([2,2])
		self.codeDef[:,7,4] = np.array([2,2])
		self.codeDef[:,0,5] = np.array([2,1])
		self.codeDef[:,6,5] = np.array([2,2])
		self.codeDef[:,0,6] = np.array([2,1])
		self.codeDef[:,1,6] = np.array([1,1])
		self.codeDef[:,6,7] = np.array([2,2])
		self.codeDef[:,7,7] = np.array([2,2])
		#Define self.Sigma for the current code
		self.Sigma = np.zeros((self.numStates,self.numStates,2))
		self.Sigma[2,0,0] = 1
		self.Sigma[3,0,1] = 1
		self.Sigma[4,1,0] = 1
		self.Sigma[2,1,1] = 1
		self.Sigma[4,2,0] = 1
		self.Sigma[5,2,1] = 1
		self.Sigma[2,3,0] = 1
		self.Sigma[3,3,1] = 1
		self.Sigma[6,4,0] = 1
		self.Sigma[7,4,1] = 1
		self.Sigma[0,5,0] = 1
		self.Sigma[6,5,1] = 1
		self.Sigma[0,6,0] = 1
		self.Sigma[1,6,1] = 1
		self.Sigma[6,7,0] = 1
		self.Sigma[7,7,1] = 1

	def run_bcjr(self, llrIn):
		numOutput = int(llrIn.shape[0]/self.n)


		#NRZI soft demapping
		llrDk = np.abs(llrIn)
		#what happens in the next line needs to be investigated
		llrDk[np.flatnonzero(np.abs(np.diff(np.sign(llrIn)))/2)+1] = -1*llrDk[np.flatnonzero(np.abs(np.diff(np.sign(llrIn)))/2)+1]
		
		#Convert LLR to probabilities LLR = log(P(X=0)/P(X=1))
		pIn = np.ones((llrIn.shape[0],2))
		pIn[:,0] = 1./(1+np.exp(-llrIn))
		pIn[:,1] = pIn[:,1] - pIn[:,0]
		
		#Reshape for more convenient processing, unfortunately Matlab uses Fortran order reshaping
		pIn = np.reshape(pIn.T,(2,self.n,numOutput), order='F')
		
		
		#Obtain transition probabilities Gamma
		Gamma = np.zeros((self.numStates,self.numStates,numOutput))
		for _iter in range(numOutput):    
			for oldState in range(self.numStates):
				for newState in range(self.numStates):
					if self.codeDef[0,newState,oldState] == 0:
						continue
					tmp = 1
					for nIter in range(self.n):
						tmp = tmp * pIn[int(self.codeDef[nIter,newState,oldState])-1,nIter,_iter]
					Gamma[newState,oldState,_iter] = tmp
		
		#Do BCJR

		#Init
		alpha = np.zeros((self.numStates,numOutput+1))
		alpha[:,0] = 1/self.numStates*np.ones(self.numStates)
		beta = np.zeros((self.numStates,numOutput+1))
		beta[:,-1] = 1/self.numStates*np.ones(self.numStates)
		app = np.zeros((numOutput,2))
		
		for i in range(numOutput):
			#Forward recursion update
			alpha[:,i+1] = Gamma[:,:,i] @ alpha[:,i]
			#Normalize
			alpha[:,i+1] = alpha[:,i+1]/np.sum(alpha[:,i+1])
		
		for i in range(numOutput-1,-1,-1):
			#Backward recursion update
			beta[:,i] = Gamma[:,:,i].transpose().dot(beta[:,i+1])
			#Normalize
			beta[:,i] = beta[:,i]/np.sum(beta[:,i])
		
		for i in range(numOutput):
			#APP update
			app[i,0] = beta[:,i+1].transpose().dot((self.Sigma[:,:,0]*Gamma[:,:,i])).dot(alpha[:,i])/(beta[:,i+1].transpose().dot(Gamma[:,:,i].dot(alpha[:,i])))
			app[i,1] = beta[:,i+1].transpose().dot((self.Sigma[:,:,1]*Gamma[:,:,i])).dot(alpha[:,i])/(beta[:,i+1].transpose().dot(Gamma[:,:,i].dot(alpha[:,i])))
		
		#Obtain LLR values
		"""for i in range(app.shape[0]):
									print(app[i,:])"""
		llrOut = np.log(app[:,0]/app[:,1])
		
		return llrOut
