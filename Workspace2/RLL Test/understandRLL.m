trellis = struct('numInputSymbols',2,'numOutputSymbols',4,...
            'numStates',4,'nextStates',[2 3;0 2;0 1;2 3],...
            'outputs',["00" "00";"01" "00";"01" "10";"00" "00"],...
            'fromStates',[1 2 inf inf;2 inf inf inf;0 1 3 inf; 0 3 inf inf]);
display(trellis.outputs)

inputbits = [0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0]';
display(inputbits')
dk_bits = rllEncoderFct(inputbits, trellis);

rll_bits = nrziEncodingFct(dk_bits);

%adding white noise
rll_bits_awgn = rll_bits;

llrOut = rllBcjrFct(rll_bits_awgn,2);

disp(llrOut)