# -*- coding: utf-8 -*-

import numpy as np
import numpy.matlib
import scipy as sp
import scipy.stats

class BCJR:
	def __init__(self, M, N, span, rolloff, rllD, snrdB, seqLen):


		self.N = N                      # Receiver oversampling
		self.M = M                      # FTN signaling rate at the transmitter
		self.span = span                # Length of the discrete filter in T_s
		self.rolloff = rolloff          # Rolloff of RRC filter
		self.rllD = rllD                # RLL d constraint
		self.rllR = 1/2                 # RLL Code Rate
		self.omega = int(self.N/self.M) # Oversampling ratio
		self.seqLen = seqLen

		# No shortening
		self.L = span*N                 # Length of symbol memory
		self.K = span*N                 # Length of noise memory

		v = rc_filter(rolloff, span, N)
		g = rrc_filter(rolloff, span, N)
		V = np.matlib.repmat(v,self.omega, 1)
		V = np.concatenate((V, np.zeros((self.omega, self.omega-1))), axis=1)
		G = np.matlib.repmat(g, self.omega,1)
		G = np.concatenate((G, np.zeros((self.omega, self.omega-1))), axis=1)
		for i in range(1,self.omega):
			V[i,:] = np.roll(V[i,:],i, axis=0)
			G[i,:] = np.roll(G[i,:],i, axis=0)

		# Obtain upsampling matrix U
		U = np.zeros(((self.L+2)*self.omega-1, self.L+1))
		for i in range((self.L+2)*self.omega-1):
			for j in range(self.L+1):
				if (i+1) == (j+1)*self.omega:
					U[i,j] = 1
				else:
					U[i,j] = 0

		
		#Obtain probabilities through numerical integration
		p_x_cond_xOld = np.zeros((2**self.L,2**self.L))
		self.Gamma1 = np.zeros((2**self.L,2**self.L))
		self.Gamma2 = np.zeros((2**self.L,2**self.L))
		
		#SNR in linear Scale
		snrLin = 10**(snrdB/10)
		
		#Optimized RLL source
		
		#Choose adjacency matrix
		if rllD == 1:
			D = np.array([[0,1],[1,1]])
		elif rllD == 2:
			D = np.array([[0,1,0],[0,0,1],[1,0,1]])
		elif rllD == 3:
			D = np.array([[0,1,0,0],[0,0,1,0],[0,0,0,1],[1,0,0,1]])
		elif rllD == 4:
			D = np.array([[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1],[1,0,0,0,1]])
		else:
			raise('Error: Specified run length constraint is not supported!')
		
		#Eigenvalues and eigen vectors
		eigVals, eigVecs = np.linalg.eig(D)
		lambda_max = np.amax(eigVals)
		idx = np.argmax(eigVals)
		b_max = eigVecs[:,idx]

		
		Prll = np.zeros(D.shape)
		for i in range(D.shape[0]):
			for j in range(D.shape[1]):
				Prll[j,i] = np.real(b_max[j]/b_max[i] * D[i,j]/lambda_max)

		
		#Generate all possible states
		stateList = np.ones((self.L,2**self.L))
		
		for i in range(2**self.L):
			binInputStr = np.binary_repr(i, width=self.L)
			stateList[:,i] = np.array(list(map(int, binInputStr)))
			#Convert to BPSK input
			stateList[stateList==0] = -1

		#Compute probabilities

		for i in range(2**self.L):               #Old state
			for j in range(2**self.L):           #New state
				
				#Check if transition is feasible mathematically
				if (stateList[1:,i] == stateList[:-1,j]).all():
					#Check if transition violates RLL constraint
					curState = np.append(stateList[:,i],stateList[-1,j])
					if np.min(np.append(np.diff(np.flatnonzero(np.diff(curState))),np.Inf)) <= rllD:
						continue #RLL violation
					
					#Update state transition probability
					if stateList[-1,j] == stateList[-2,j]:
						#No transition
						p_x_cond_xOld[j,i] = Prll[-1,-1]
					else:
						#Transition
						p_x_cond_xOld[j,i] = Prll[0,-1]
					
					#Forney style system model with uncorrelated noise
					curMean = np.sqrt(snrLin)*V@U@np.append(stateList[:,i],stateList[-1,j])
					tmpProb = sp.stats.norm.sf(curMean)
					self.Gamma1[j,i] = tmpProb          # "-1"
					self.Gamma2[j,i] = 1-tmpProb        # "+1"
		
		
		# Include transition probabilities
		self.Gamma1 = self.Gamma1 * p_x_cond_xOld
		self.Gamma2 = self.Gamma2 * p_x_cond_xOld

		
		# Define summation matrix
		self.Sigma = np.zeros((2**self.L, 2**self.L, 2))
		for i in range(2**self.L):
			if np.mod(i,2) == 1:
				self.Sigma[i,:,1] = np.ones((1,2**self.L))
			else:
				self.Sigma[i,:,0] = np.ones((1,2**self.L))
		
		# Prune matrices to avoid unused states
		valMat = np.nonzero(p_x_cond_xOld)
		validStateList = np.unique(valMat)
		self.numValidStates = np.prod(validStateList.shape)
		
		#Overwrite Gammas and self.Sigma
		self.Gamma1 = self.Gamma1[validStateList,:][:,validStateList]
		self.Gamma2 = self.Gamma2[validStateList,:][:,validStateList]
		self.Sigma = self.Sigma[validStateList,:,:][:,validStateList,:]


	def run(self, gammaIdx):
		# BCJR MAP sequence detection

		# Re-Init
		alpha = np.zeros((self.numValidStates,self.seqLen+self.L+self.rllD+1))
		alpha[0,0] = 1
		beta = np.zeros((self.numValidStates,self.seqLen+self.L+self.rllD+1))
		beta[0,-1] = 1
		app = np.zeros((2,self.seqLen+self.L+self.rllD+1))
		app[1,-1] = 1

		for i in range(self.seqLen+self.L+self.rllD):
			if gammaIdx[i] == -1:
				alpha[:,i+1] = self.Gamma1 @ alpha[:,i]
			elif gammaIdx[i] == 1:
				alpha[:,i+1] = self.Gamma2 @ alpha[:,i]
			# Normalize
			alpha[:,i+1] = alpha[:,i+1]/np.sum(alpha[:,i+1])
		

		for i in range(self.seqLen+self.L+self.rllD-1,-1,-1):
			if gammaIdx[i] == -1:
				beta[:,i] = self.Gamma1.T @ beta[:,i+1]
			elif gammaIdx[i] == 1:
				beta[:,i] = self.Gamma2.T @ beta[:,i+1]
			# Normalize
			beta[:,i] = beta[:,i]/np.sum(beta[:,i])

		for i in range(self.seqLen+self.L+self.rllD):
			if gammaIdx[i] == -1:
				app[0,i] = beta[:,i+1].T@(self.Sigma[:,:,0]*self.Gamma1)@alpha[:,i]/(beta[:,i+1].T@self.Gamma1@alpha[:,i])
				app[1,i] = beta[:,i+1].T@(self.Sigma[:,:,1]*self.Gamma1)@alpha[:,i]/(beta[:,i+1].T@self.Gamma1@alpha[:,i])
			elif gammaIdx[i] == 1:
				app[0,i] = beta[:,i+1].T@(self.Sigma[:,:,0]*self.Gamma2)@alpha[:,i]/(beta[:,i+1].T@self.Gamma2@alpha[:,i])
				app[1,i] = beta[:,i+1].T@(self.Sigma[:,:,1]*self.Gamma2)@alpha[:,i]/(beta[:,i+1].T@self.Gamma2@alpha[:,i])

		# Obtain LLR values
		llr = np.log(app[0,:]/app[1,:])
			
		# Shorten
		llr = llr[:self.seqLen]

		return llr


	



#raised cosine filter for interpolating between taps of the channel response h_c(t)
def rc_filter(roll_off, filter_length, sampling_freq):
	t = np.linspace(-filter_length/2, filter_length/2, num=(filter_length*sampling_freq+1))
	rc = np.piecewise(t, [t==0, np.abs(t) == 1/float(2*roll_off), (t!=0) & (np.abs(t) != 1/float(2*roll_off))], [lambda t:1,  
					  lambda t: np.pi/4*np.sin(np.pi/(2*roll_off))/(np.pi/(2*roll_off)), 
					  lambda t:np.sin(np.pi*t)/(np.pi*t)*np.cos(roll_off*np.pi*t)/(1-(2*roll_off*t)**2)])

	#matlab normailzes the result, s.t. the sum of the squared samples is one
	power = np.sum(rc**2)
	rc /= np.sqrt(power)
	return rc

#the noise must be convolved with only one filter
def rrc_filter(roll_off, filter_length, sampling_freq):
	t = np.linspace(-filter_length/2, filter_length/2, num=(filter_length*sampling_freq+1))
	rrc = np.piecewise(t, [t==0, np.abs(t) == 1/float(4*roll_off), (t!=0) & (np.abs(t) != 1/float(4*roll_off))], [
						lambda t:(1-roll_off+4*roll_off/np.pi), 
						lambda t: roll_off/np.sqrt(2)*((1+2/np.pi)*np.sin(np.pi/(4*roll_off))+(1-2/np.pi)*np.cos(np.pi/(4*roll_off))),
						lambda t: (np.sin(np.pi*t*(1-roll_off))+4*roll_off*t*np.cos(np.pi*t*(1+roll_off)))/(np.pi*t*(1-(4*roll_off*t)**2))])
	

	#matlab normailzes the result, s.t. the sum of the squared samples is one
	power = np.sum(rrc**2)
	rrc /= np.sqrt(power)
	return rrc



if __name__ == "__main__":
	bcjr = BCJR(M =2, N=2, span=2, rolloff=0.5, rllD=2, fecR=0.5, ebno=10, seqLen=11264)