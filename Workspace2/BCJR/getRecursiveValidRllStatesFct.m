function retVal = getRecursiveValidRllStatesFct(vec, i, N, rllD)
%RECURSIVEVALIDRLLFCT Recursively determine all valid RLL states. The idea
% is to build a recursive tree structure, which just follows the branches
% which correspond to valid RLL code words. If the desired depth of 'N' is
% reached the algorithm terminates and returns all valid code words to the 
% root of the tree.

if i == 0   % First function call
    retVal = [getRecursiveValidRllStatesFct( 1, i+1, N, rllD), ...
            getRecursiveValidRllStatesFct( -1, i+1, N, rllD)];
else        % Subsequent function calls go here
    % Is the input valid?
    if min([diff(find(diff(vec)));Inf]) <= rllD
        % RLL violation, return empty
        retVal = []; 
    else
        if i == N   % Are we done yet?
            retVal = vec;
        else        % We have to go deeper!
            retVal = [getRecursiveValidRllStatesFct([vec;1], i+1, N, rllD), ...
                getRecursiveValidRllStatesFct([vec;-1], i+1, N, rllD)];
        end
    end
end

end

