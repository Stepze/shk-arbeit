function [ber, ebno, dur] = evalCodedBerBcjrFct( seed, ebno, N, M, span, ...
    rolloff, rllD, corrType, fecR, numSim, minNumErr )
% Evaluate SER based on MAP symbol estimation using the 
% BCJR algorithm.


%% Input conversion
% This is necessary because command line arguments are passed as
% string; within MATLAB it is not necessary

if ischar(seed)
    seed = str2double(seed);            % Seed value for random number generator
    ebno = str2double(ebno);            % Ebno value
    N = str2double(N);                  % Receiver oversampling
    M = str2double(M);                  % FTN signaling rate at the transmitter
    span = str2double(span);            % Length of the discrete filter in T_s
    rolloff = str2double(rolloff);      % Rolloff of RRC filter
    rllD = str2double(rllD);            % RLL d constraint
    fecR = str2double(fecR);            % Code rate of the channel code
    numSim = str2double(numSim);        % Number of simulations
    minNumErr = str2double(minNumErr);  % Minimum number of errors for early termination
end


%% Init
trellis = struct('numInputSymbols',2,'numOutputSymbols',4,...
    'numStates',4,'nextStates',[2 3;0 2;0 1;2 3],...
    'outputs',["00" "00";"01" "00";"01" "10";"00" "00"],...
    'fromStates',[1 2 inf inf;2 inf inf inf;0 1 3 inf; 0 3 inf inf]);

tic;
rllR = 1/2;
fecN = 8448;
seqLen = ceil(fecN/(3*fecR*rllR));
disp(seqLen)
pause()
%N muss raus
snrdB = ebno + 10*log10(fecR) + 10*log10(rllR);
rng(seed);                  % Set seed of random number generator
omega = N/M;                % Oversampling ratio
snrLin = 10^(snrdB/10);     % Convert to linear SNR

% Get filters
v = rcosdesign(rolloff,span,N,'normal');
g = rcosdesign(rolloff,span,N,'sqrt');

% No shortening
L = span*N;         % Length of symbol memory
K = span*N;         % Length of noise memory

% Filter matrices
V = repmat(v,omega,1);
G = repmat(g,omega,1);

V = [V,zeros(omega,omega-1)];
G = [G,zeros(omega,omega-1)];
for i = 2:omega
    V(i,:) = circshift(V(i,:),i-1);
end
for i = 2:omega
    G(i,:) = circshift(G(i,:),i-1);
end

% Obtain upsampling matrix U
for i = 1:(L+2)*omega-1
    for j = 1:L+1
        if i == j*omega
            U(i,j) = 1;
        else
            U(i,j) = 0;
        end
    end
end

%% Obtain probabilities through numerical integration
p_x_cond_xOld = sparse(2^L,2^L);
Gamma1 = sparse(2^L,2^L);
Gamma2 = sparse(2^L,2^L);

% Optimized RLL source

% Choose adjacency matrix
if rllD == 1
    D = [0,1;1,1];
elseif rllD == 2
    D = [0,1,0;0,0,1;1,0,1];
elseif rllD == 3
    D = [0,1,0,0;0,0,1,0;0,0,0,1;1,0,0,1];
elseif rllD == 4
    D = [0,1,0,0,0;0,0,1,0,0;0,0,0,1,0;0,0,0,0,1;1,0,0,0,1];
else
    error('Error: Specified run length constraint rllD=%d is not supported!',rllD);
end

% Eigenvalues and eigen vectors
[eigVecs, eigDiag] = eig(D);
[lambda, idx] = max(diag(eigDiag));
b = eigVecs(:,idx);

for i = 1:size(D,1)
    for j = 1:size(D,2)
        Prll(j,i) = b(j)./b(i) .* D(i,j)./lambda;
    end
end


% Generate all possible states
stateList = ones(L,2^L);
for i = 1:2^L
    binInputStr = dec2bin(i-1);
    while size(binInputStr,2) < L
        binInputStr = ['0', binInputStr];
    end
    % Convert to BPSK input
    stateList(binInputStr=='0',i)=-1;
end

% Compute probabilities
for i = 1:2^L               % Old state
    for j = 1:2^L           % New state
        
        % Check if transition is feasible mathematically
        if stateList(2:end,i) == stateList(1:end-1,j)
            
            % Check if transition violates RLL constraint
            curState = [stateList(:,i);stateList(end,j)];
            if min([diff(find(diff(curState)));Inf]) <= rllD
                continue; % RLL violation
            end
            
            % Update state transition probability
            if stateList(end,j) == stateList(end-1,j)
                % No transition
                p_x_cond_xOld(j,i) = Prll(end,end);
            else
                % Transition
                p_x_cond_xOld(j,i) = Prll(1,end);
            end
            
            % Forney style system model with uncorrelated noise
            curMean = sqrt(snrLin).* V*U*[stateList(:,i);stateList(end,j)];
            tmpProb = qfunc(curMean);
            Gamma1(j,i) = tmpProb;          % "-1"
            Gamma2(j,i) = 1-tmpProb;        % "+1"
            
        end
    end
end

% Include transition probabilities
Gamma1 = Gamma1 .* p_x_cond_xOld;
Gamma2 = Gamma2 .* p_x_cond_xOld;

%% Simulate setup
y = zeros(seqLen+2*L+rllD, 1);
r = zeros(seqLen+2*L+rllD, 1);
totNumErr = zeros(2,1);
totNumErrUncoded = 0;
totNumBits = 0;
totNumUncodedBits = 0;

% Define summation matrix
Sigma = zeros(2^L,2^L,2);
for i = 1:2^L
    if mod(i,2)==0
        Sigma(i,:,2) = ones(1,2^L);
    else
        Sigma(i,:,1) = ones(1,2^L);
    end
end


%% Prune matrices to avoid unused states
[valRow,valCol] = find(p_x_cond_xOld);
validStateList = unique([valRow;valCol]);
numValidStates = numel(validStateList);


% Overwrite Gammas and Sigma
Gamma1 = sparse(Gamma1(validStateList,validStateList));
Gamma2 = sparse(Gamma2(validStateList,validStateList));
Sigma = Sigma(validStateList,validStateList,:);


%% Simulation
for iter = 1:numSim
    
    outIter = 0;
    
    bits = randi([0,1],fecN/3,1);
    bitsEncFec = nrLDPCEncode(bits,1);
    % Puncturing
    bitsRatematched = bitsEncFec(1:ceil(length(bits)/fecR));
    bitsEncFecEncRll = rllEncoderFct(bitsRatematched, trellis);
    x = nrziEncodingFct(bitsEncFecEncRll);
    % Make sure to start and terminate in known state
    x = [-ones(L,1);x;repmat(x(end),rllD,1);-ones(L+1,1)];
    n = randn(seqLen+2*K+rllD+1,1);                 % Generate n 
    

    for i = 1+L:seqLen+2*L+rllD+1

        outIter = outIter + 1;

        if strcmp(corrType,'corr')
            % Correlated noise
            y(outIter) = sqrt(snrLin).*V*U*x(i-L:i) + G*n(i-L:i+(K-L));
        elseif strcmp(corrType,'uncorr')
            % Uncorrelated noise
            y(outIter) = sqrt(snrLin).*V*U*x(i-L:i) + n(i);
        end
        r(outIter) = sign(y(outIter));

        % Choose Gamma based on current observation
        gammaIdx(outIter) = r(outIter);

        % Store sequence
        xSeq(outIter) = x(i);

    end
    %% BCJR MAP sequence detection

    % Re-Init
    alpha = zeros(numValidStates,seqLen+L+rllD+1);
    alpha(1,1) = 1;
    beta = zeros(numValidStates,seqLen+L+rllD+1);
    beta(1,end) = 1;
    app = zeros(2,seqLen+L+rllD+1);
    app(1,end) = 1;

    for i = 1:seqLen+L+rllD
        if gammaIdx(i) == -1
            alpha(:,i+1) = Gamma1 * alpha(:,i);
        elseif gammaIdx(i) == +1
            alpha(:,i+1) = Gamma2 * alpha(:,i);
        end
        % Normalize
        alpha(:,i+1) = alpha(:,i+1)./sum(alpha(:,i+1));
    end

    for i = seqLen+L+rllD:-1:1
        if gammaIdx(i) == -1
            beta(:,i) = Gamma1' * beta(:,i+1);
        elseif gammaIdx(i) == +1
            beta(:,i) = Gamma2' * beta(:,i+1);
        end
        % Normalize
        beta(:,i) = beta(:,i)./sum(beta(:,i));
    end
    
    for i = 1:seqLen+L+rllD
        if gammaIdx(i) == -1
            app(1,i) = beta(:,i+1)'*(Sigma(:,:,1).*Gamma1)*alpha(:,i)/(beta(:,i+1)'*Gamma1*alpha(:,i));
            app(2,i) = beta(:,i+1)'*(Sigma(:,:,2).*Gamma1)*alpha(:,i)/(beta(:,i+1)'*Gamma1*alpha(:,i));
        elseif gammaIdx(i) == +1
            app(1,i) = beta(:,i+1)'*(Sigma(:,:,1).*Gamma2)*alpha(:,i)/(beta(:,i+1)'*Gamma2*alpha(:,i));
            app(2,i) = beta(:,i+1)'*(Sigma(:,:,2).*Gamma2)*alpha(:,i)/(beta(:,i+1)'*Gamma2*alpha(:,i));
        end
    end
    
    disp("app")
    disp(app(1,1:10))
    disp(app(2,1:10))
    % Obtain LLR values
    llr = log(app(1,:)./app(2,:));
    
    % Shorten
    llr = llr(1:seqLen);
    
    
    
    %% Decode

    % Hard
    llrHard = sign(llr);
    dk_received = nrziDecodingFct(llrHard')';
    Pe = qfunc(sqrt(2*10^(ebno/10)));
    llrMag = log10((1-Pe)/Pe);
    receivedBits = llrMag.*(1-2*rllViterbiFct(dk_received,trellis)');
    % Rate recovery
    receivedRec = [receivedBits;zeros(fecN-length(receivedBits),1)];
    [bitsDecHard,~,~] = nrLDPCDecode(receivedRec,1,50);
    
    % Soft
    bitsEncFecDecRll = rllBcjrFct(llr', 2);
    disp('size llr_rll')
    disp(size(bitsEncFecDecRll))
    pause()
    % Rate recovery
    receivedRec = [bitsEncFecDecRll;zeros(fecN-length(bitsEncFecDecRll),1)];
    [bitsDecSoft,~,~] = nrLDPCDecode(receivedRec,1,50);
   
    % Update error statistics
    totNumErr(1) = totNumErr(1) + numel(find(bitsDecHard ~= bits));
    totNumErr(2) = totNumErr(2) + numel(find(bitsDecSoft ~= bits));
    totNumErrUncoded = totNumErrUncoded + numel(find(-llrHard ~= xSeq(1:seqLen)));
    totNumBits = totNumBits + length(bits);
    totNumUncodedBits = totNumUncodedBits + seqLen;
    
    % Early termination
    if min(totNumErr) > minNumErr
        break;
    end
    
end

% Compute SER
ber = totNumErr/totNumBits
uncodedBer = totNumErrUncoded / totNumUncodedBits
dur = toc

%% Write to HDD
% Create a directory and file name to store the data
mkdir('./data');
% Create strings for file name without '.'
rolloffStr = strrep(num2str(rolloff),'.','-');
ebnoStr = strrep(num2str(ebno),'.','-');
fecRStr = strrep(num2str(fecR),'.','-');
% Create file name
fileName = ['./data/data_', num2str(seed), '_N', num2str(N), '_M', num2str(M), ...
    '_span', num2str(span), '_rolloff', rolloffStr, '_', corrType, ...
    '_rllD', num2str(rllD),'_fecR', fecRStr, '_ebno', ebnoStr];
% Clean workspace to reduce disk space
clear x* y r n alpha beta Gamma p*
% Save whole workspace
save(fileName);