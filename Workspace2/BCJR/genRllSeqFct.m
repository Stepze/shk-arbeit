function [ dataRll ] = genRllSeqFct( rllD, nSym )
%GENRLLSEQFCT This function generates a binary RLL sequence of length 
% 'nSym' and with d-constraint 'rllD'.

%% Init
dataDk = zeros(nSym,1);
dataRll = zeros(nSym,1);

%% Choose adjacency matrix
if rllD == 1
    D = [0,1;1,1];
elseif rllD == 2
    D = [0,1,0;0,0,1;1,0,1];
elseif rllD == 3
    D = [0,1,0,0;0,0,1,0;0,0,0,1;1,0,0,1];
elseif rllD == 4
    D = [0,1,0,0,0;0,0,1,0,0;0,0,0,1,0;0,0,0,0,1;1,0,0,0,1];
else
    error('Error: Specified run length constraint rllD=%d is not supported!',rllD);
end

%% Eigenvalues and eigen vectors
[eigVecs, eigDiag] = eig(D);
[lambda, idx] = max(diag(eigDiag));
b = eigVecs(:,idx);

for i = 1:size(D,1)
    for j = 1:size(D,2)
        Prll(i,j) = b(j)./b(i) .* D(i,j)./lambda;
    end
end

%% Simulate walk on markov chain to generate dk sequence
dataHmm = hmmgenerate(nSym, Prll', eye(length(Prll)));
dataDk(dataHmm == rllD) = 1;

%% Convert to RLL
curVal = -1;
for i = 1:length(dataDk)
    if dataDk(i) == 1
        curVal = -curVal;
    end
    dataRll(i)=curVal;
end

end

