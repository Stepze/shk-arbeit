%Viterbi
function decodedBits=rllViterbiFct(inputBits, trellis)
%initialization
Rate=0.5;
metrics=zeros(trellis.numStates, length(inputBits)*Rate+1);
metrics(1:end,1:end)=Inf; % to avoid errors; maybe delete this in the future
metrics(:,1)=0; %metric at t=1 is 0
Survivor=zeros(trellis.numStates, length(inputBits)*Rate+1);

% Construct output
trellisOutputsDouble = zeros(2,trellis.numStates,trellis.numInputSymbols);
for curInputSym = 1:trellis.numInputSymbols
    for curState = 1:trellis.numStates
        tmp = char(trellis.outputs(curState,curInputSym));
        for curOutputBit = 1:log2(trellis.numOutputSymbols)
            trellisOutputsDouble(curOutputBit,curState,curInputSym) = str2double(tmp(curOutputBit));
        end
    end
end

distance=zeros(1,trellis.numStates);
distance(1:end)=inf;

%loop over time
for t=2:1:length(inputBits)*Rate+1%immer zwei bits
    
    %get codeword from inputBits
    codeword = transpose(inputBits(2*(t-1)-1:2*(t-1)));
    
    %loop over states
    for k=0:trellis.numStates-1
        %initialize distance vector again
        distance(:)=inf;
        for l=1:trellis.numStates
            
            if(trellis.fromStates(k+1,l)~=Inf)
                d=trellis.fromStates(k+1,l); %which statetransition (from d->k)
                %first get the place in the matrix, where the output is
                %written:
                I = find(trellis.nextStates(d+1,:)==k);
                %calculating the hamming distance
                distance(l) = sum(abs(trellisOutputsDouble(:,d+1,I)-codeword)) + metrics(d+1,t-1);
            end
        end
        %choose smallest metric
        [~, minimumindex] = min(distance);
        %choose winner
        Survivor(k+1,t)=trellis.fromStates(k+1,minimumindex);
        resultingdistance=distance(minimumindex);
        %update Metric-Matrix
        metrics(k+1,t)=resultingdistance;
    end
end

%Trace Back
trace=zeros(1,length(inputBits)*Rate+1);
%first choose the smallest metric
start=find(metrics(:,end)==min(metrics(:,end)));
%if because it is possible to have two smallest metrics
if (length(start)==1)
    trace(end)=start-1;
else
    trace(end)=start(1)-1;
    
end
for i=length(trace)-1:-1:1
    trace(i)=Survivor(trace(i+1)+1,i+1);
end

%Read required input to reach this state transitions:
decodedBits=zeros(1,length(inputBits)*Rate);
for i=1:length(trace)-1
    index=find(trellis.nextStates(trace(i)+1,:)==trace(i+1));
    decodedBits(i)=index-1; %since index=1 --> 0 input and index=2 --> 1 input
end
end