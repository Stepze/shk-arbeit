function [validTransitions, validStateList] = getValidRllTransitionsFct(L, rllD)
%GETVALIDRLLTRANSITIONSFCT Function returns a vector with all transitions
% which do not violate the specified RLL constraint. 

%% Init
memLen = 2*L;
validTransitions = [];

%% Check if the result is already precomputed
% Create file name
fileName = ['./RllStateData/validTransitions_L', num2str(L), '_rllD', num2str(rllD), '.mat'];
% Check if file exists
if isfile(fileName)
    loadStruct = load(fileName);
    validTransitions = loadStruct.validTransitions;
    validStateList = loadStruct.validStateList;
    return;
end

%% Find all valid real-valued RLL states
tmpValidStates = getRecursiveValidRllStatesFct(0, 0, L,rllD);
validStateList = zeros(memLen,size(tmpValidStates,2)^2);

%% Find all valid complex-valued RLL states
for i = 1:size(tmpValidStates,2)
    for j = 1:size(tmpValidStates,2)
        validStateList(:,(i-1)*size(tmpValidStates,2)+j) = [tmpValidStates(:,i);tmpValidStates(:,j)];
    end
end

%% Try all possible complex-valued RLL states transitions and save the valid ones
for i = 1:size(validStateList,2)
    for j = 1:size(validStateList,2)
        
        oldStateReal = validStateList(1: L,i);
        oldStateImag = validStateList(L+1:end,i);
        newStateReal = validStateList(1:L,j);
        newStateImag = validStateList(L+1:end,j);
        
        % Check if transition is feasible mathematically
        if all(oldStateReal(2:end) == newStateReal(1:end-1)) && ...
                all(oldStateImag(2:end) == newStateImag(1:end-1))
            
            % Check if transition violates RLL constraint
            % Check real
            curStateReal = [oldStateReal;newStateReal(end)];
            if min([diff(find(diff(curStateReal)));Inf]) <=  rllD
                continue; % RLL violation
            end
            %Check imag
            curStateImag = [oldStateImag;newStateImag(end)];
            if min([diff(find(diff(curStateImag)));Inf]) <=  rllD
                continue; % RLL violation
            end
            
            % Save valid transition
            validTransitions = [validTransitions; i, j];
            
        end
    end
end

%% Store result to HDD to avoid second precomputation
% Create a directory and file name to store the data
mkdir('./RllStateData');

% Save variables
save(fileName,'validTransitions', 'validStateList');

end

