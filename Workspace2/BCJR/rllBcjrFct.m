function [llrOut] = rllBcjrFct(llrIn, rllD)
%RLLBCJRFCT This function performs soft-input soft-output demapping of an 
% RLL sequence.

%% Init

if rllD == 2
    
    % Hard coding for RLL code with R=1/2
    k = 1;
    n = 2;
    numStates = 8;
    
    % Define code
    % 1 indicates '-1' and 2 indicates '+1'
    codeDef = zeros(n,numStates,numStates);
    codeDef(:,3,1) = [1;1];
    codeDef(:,4,1) = [1;1];
    codeDef(:,5,2) = [1;2];
    codeDef(:,3,2) = [1;1];
    codeDef(:,5,3) = [1;2];
    codeDef(:,6,3) = [2;2];
    codeDef(:,3,4) = [1;1];
    codeDef(:,4,4) = [1;1];
    codeDef(:,7,5) = [2;2];
    codeDef(:,8,5) = [2;2];
    codeDef(:,1,6) = [2;1];
    codeDef(:,7,6) = [2;2];
    codeDef(:,1,7) = [2;1];
    codeDef(:,2,7) = [1;1];
    codeDef(:,7,8) = [2;2];
    codeDef(:,8,8) = [2;2];

    % Define Sigma for the current code
    Sigma = zeros(numStates,numStates,2);
    Sigma(3,1,1) = 1;
    Sigma(4,1,2) = 1;
    Sigma(5,2,1) = 1;
    Sigma(3,2,2) = 1;
    Sigma(5,3,1) = 1;
    Sigma(6,3,2) = 1;
    Sigma(3,4,1) = 1;
    Sigma(4,4,2) = 1;
    Sigma(7,5,1) = 1;
    Sigma(8,5,2) = 1;
    Sigma(1,6,1) = 1;
    Sigma(7,6,2) = 1;
    Sigma(1,7,1) = 1;
    Sigma(2,7,2) = 1;
    Sigma(7,8,1) = 1;
    Sigma(8,8,2) = 1;
    
else
    
    error("RLL d constraint 'd=%d' is not supported!",rllD);
    
end

numOutput = size(llrIn,1)/n;


%% NRZI soft demapping
llrDk = abs(llrIn);
llrDk(find(abs(diff(sign(llrIn)))/2)+1) = -1.* llrDk(find(abs(diff(sign(llrIn)))/2)+1);

%% Convert LLR to probabilities LLR = log(P(X=0)/P(X=1))
pIn = ones(size(llrIn,1),2);
pIn(:,1) = 1./(1+exp(-llrIn));
pIn(:,2) = pIn(:,2) - pIn(:,1);

% Reshape for more convenient processing
pIn = reshape(pIn',2,n,numOutput);


%% Obtain transition probabilities Gamma
Gamma = zeros(numStates,numStates,numOutput);
for iter = 1:numOutput
    
    for oldState = 1:numStates
        for newState = 1:numStates
            if codeDef(1,newState,oldState) == 0
                continue;
            end
            tmp = 1;
            for nIter = 1:n
                tmp = tmp * pIn(codeDef(nIter,newState,oldState),nIter,iter);
            end
            Gamma(newState,oldState,iter) = tmp;
        end
    end
    
end
%% Do BCJR

% Init
alpha = zeros(numStates,numOutput+1);
alpha(:,1) = 1/numStates.*ones(numStates,1);
beta = zeros(numStates,numOutput+1);
beta(:,end) = 1/numStates.*ones(numStates,1);
app = zeros(numOutput,2);

for i = 1:numOutput
    % Forward recursion update
    alpha(:,i+1) = Gamma(:,:,i) * alpha(:,i);
    % Normalize
    alpha(:,i+1) = alpha(:,i+1)./sum(alpha(:,i+1));
end

for i = numOutput:-1:1
    % Forward recursion update
    beta(:,i) = Gamma(:,:,i)' * beta(:,i+1);
    % Normalize
    beta(:,i) = beta(:,i)./sum(beta(:,i));
end

for i = 1:numOutput
    % APP update
    app(i,1) = beta(:,i+1)'*(Sigma(:,:,1).*Gamma(:,:,i))*alpha(:,i)/(beta(:,i+1)'*Gamma(:,:,i)*alpha(:,i));
    app(i,2) = beta(:,i+1)'*(Sigma(:,:,2).*Gamma(:,:,i))*alpha(:,i)/(beta(:,i+1)'*Gamma(:,:,i)*alpha(:,i));
end

% Obtain LLR values
llrOut = log(app(:,1)./app(:,2));


end