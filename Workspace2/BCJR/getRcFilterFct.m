function f = getRcFilterFct( rolloff, span, Mrx, filterType, offset )
%GETRCFILTERFCT Compute raised- or root-raised-cosine filter. Note: The
% filter f is not normalized!

%% Init
Ts = 1;
tArray = [-span/2:1/Mrx:span/2] - offset;
f = zeros(1,length(tArray));

%% Compute filter
for i = 1:length(tArray)
    t = tArray(i);
    switch filterType
        case 'rc'
            if t == 0
                f(i) = 1;
            elseif abs(t) == Ts/(2*rolloff)
                f(i) = 2*rolloff./(4*Ts) .* sin(pi/(2*rolloff));
            else
                f(i) = sin(pi.*t./Ts)./(pi/Ts.*t) .* (cos(rolloff*pi.*t./Ts))./(1-(2*rolloff/Ts .* t).^2);
            end
        case 'rrc'
            if t == 0
                f(i) = 1/Ts*(1+rolloff*(4/pi-1));
            elseif abs(t) == Ts/(4*rolloff)
                f(i) = rolloff/(sqrt(2)*Ts) .*((1+2/pi)*sin(pi/(4*rolloff))+(1-2/pi)*cos(pi/(4*rolloff)));
            else
                f(i) = (sin(pi.*t./Ts*(1-rolloff))+4*rolloff*t/Ts*cos(pi.*t./Ts*(1+rolloff)))/((pi/Ts.*t)*(1-(4*rolloff*t/Ts).^2));
            end
        otherwise
            error("Error: Unknown filter type '%s'.", filterType);
    end
end

end

