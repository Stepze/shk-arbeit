function encodedBits = rllEncoderFct(inputBits, trellis)

%ENCODEDBITS Encodes incoming bitstream with a given trellis structure.
%   Please note that the trellis structure needs to be like the one in the 
%   example in order to decode trellis structures that do not satisfy the 
%   conditions of convolutional codes.
%   Example:
%
%     trellis = struct('numInputSymbols',2,'numOutputSymbols',4,...
%     'numStates',4,'nextStates',[2 3;0 2;0 1;2 3],...
%     'outputs',["00" "00";"01" "00";"01" "10";"00" "00"],...
%     'fromStates',[1 2 inf inf;2 inf inf inf;0 1 3 inf; 0 3 inf inf]);
%
%   Please note that there is an additional field: fromStates and that the outputs
%   are given as strings


%% Init
currentState = 0;
encodedBits = zeros(2*length(inputBits),1);
% Construct output in doubles
trellisOutputsDouble = zeros(2,trellis.numStates,trellis.numInputSymbols);
for curInputSym = 1:trellis.numInputSymbols
    for curState = 1:trellis.numStates
        tmp = char(trellis.outputs(curState,curInputSym));
        for curOutputBit = 1:log2(trellis.numOutputSymbols)
            trellisOutputsDouble(curOutputBit,curState,curInputSym) = str2double(tmp(curOutputBit));
        end
    end
end


%% Encode
for i=1:length(inputBits)
    nextState = trellis.nextStates(currentState+1,inputBits(i)+1);
    encodedBits((i-1)*2+1:i*2) = trellisOutputsDouble(:,currentState+1,inputBits(i)+1);
    currentState = nextState;
end

end