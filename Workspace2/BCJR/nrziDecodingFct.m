function DecodedBits = nrziDecodingFct(CodedBits)

% if level change       --> 1
% if no level change    --> 0

% First generate vorrect size matrix
DecodedBits=zeros(size(CodedBits,1),size(CodedBits,2));

% Initialize first bits with 0
DecodedBits(1,:)=0;

% Differential decoding:
for i=2:size(DecodedBits,1)
    
    for j=1:size(DecodedBits,2)
        
        if (CodedBits(i,j)== CodedBits(i-1,j))
            DecodedBits(i,j)=0;
        else
            DecodedBits(i,j)=1;
        end
        
    end
    
end

end