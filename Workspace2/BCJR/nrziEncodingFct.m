function CodedBits = nrziEncodingFct(InputBits)

% 1--> level Change
% 0--> no level change

%First Generate Correct Size Matrix
CodedBits=zeros(size(InputBits,1),size(InputBits,2));

%initialize First Bit with level -1
CodedBits(1,:)=-1;

%Differential Coding:
for i=2:size(CodedBits,1)
    for j=1:size(CodedBits,2)
        
        if(InputBits(i,j)== 1)
            CodedBits(i,j)= -CodedBits(i-1,j);
        else
            CodedBits(i,j)=CodedBits(i-1,j);
        end
    end
end

end