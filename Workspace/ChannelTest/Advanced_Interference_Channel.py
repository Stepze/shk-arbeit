import numpy as np

#This class implements a transmission channel model, which supports pseudo-analog signals
#Intersymbolinterference can be achieved by setting the channels memory
#Moreover faster than Nyquist signaling and receiving is supported 

class Advanced_Interference_Channel:
    def __init__(self, noise_variance = 0.001, memory_length = 2, memory_coefficients = [1,0.5],
                 transmission_filter_response_function=None, receive_filter_response_function=None,
                 oversampling_factor=5):
        
        #approximated sinc filter as standard impulse response for the transmission filter 
        if transmission_filter_response_function==None:  
            self.transmission_filter_fn = self.sinc_filter_fn
        else:
            self.transmission_filter_fn = transmission_filter_response_function
            
        if receive_filter_response_function == None:
            self.receive_filter_fn = self.sinc_filter_fn
        else:
            self.receive_filter_fn = receive_filter_response_function
            
        #factor for faster than Nyqist signaling
        self.signaling_factor = 1
        
        #variance of the noise, which is added at the output
        self.noise_variance = noise_variance
        
        #checking whether there is the right number of coefficients for the length of the channel
        #the direct path (with no delay) is described by memory_coefficients[0]
        assert memory_length == len(memory_coefficients)
        self.memory_length = memory_length
        self.memory_coefficients = memory_coefficients
        
        #creating the "memory" of the channel and initialize it to zero
        self.symbol_memory = [0]*memory_length
        
        #minimal difference in noise variance at the output is caused by noise being filtered
        #with an ideal low-pass in the non-oversampling case versus an suboptimal low-pass
        #in the case of oversampling
        self.oversampling_fact = oversampling_factor
        
        #ensure that a sequence can only be received one time
        self.last_sequence_received = True 
        #symbol rate, at which the sequence was sent
        self.T_s = 0
        #symbol rate at which the channel output is received
        self.T_r = 0
    
        self.last_sequence_received = True
    
    def sinc_filter_fn(self, symbol_period, sampling_freq):
        t = np.arange(-10*symbol_period,10*symbol_period,1/float(sampling_freq))
        
        #Achtung!!!! andere Implementierung von sinc() in Numpy als in Mathematica!
        return 1/float(symbol_period) * np.sinc(t/float(symbol_period))
    
    
    
    #sequence - sequence to be sent
    #T_s - symbol period
    #signaling_factor - the period of the transmission filter will be T_s/signaling_factor
    
    def transmit(self, sequence, T_s,signaling_factor = 1):
        assert self.last_sequence_received == True
        if signaling_factor != 1:
            self.signaling_factor = signaling_factor
        
        self.sequence = sequence
        self.T_s = T_s
        self.last_sequence_received = False
        
    def receive(self, T_r):
        #make sure that no sequence can be received more than once
        assert self.last_sequence_received == False
        #make sure the channel output is not sampled higher than allowed
        assert self.oversampling_fact >= self.T_s/float(T_r)
        
        #creating the actual impulse response array
        imp_resp_transmit = self.transmission_filter_fn(self.T_s, self.oversampling_fact)
        
        imp_resp_receive = self.receive_filter_fn(self.T_s, self.oversampling_fact)
        
        
        #the actual distance of the symbols has to be adapted for the convolution according to the chosen
        #sampling frequency
        self.sequence = self.sequence.reshape(len(self.sequence),-1)
        self.sequence = np.pad(self.sequence,[(0,0),(0,int(self.oversampling_fact/self.signaling_factor)-1)],
                               'constant',constant_values=(0))
        self.sequence = self.sequence.reshape(-1)
        
        #first convolution
        conv_seq1 = np.convolve(self.sequence, imp_resp_transmit, 'Full')
        
        #for self.memory_coefficients it is the same as with self.sequence
        self.mem_coeffs = np.array(self.memory_coefficients)
        self.mem_coeffs = self.mem_coeffs.reshape(len(self.mem_coeffs),-1)
        self.mem_coeffs = np.pad(self.mem_coeffs,[(0,0),(0,int(self.oversampling_fact)-1)],
                                          'constant',constant_values=(0))
        self.mem_coeffs = self.mem_coeffs.reshape(-1)
        
        
        #second convolution
        conv_seq2 = np.convolve(conv_seq1, self.mem_coeffs, 'Full')
        
       
        #then the addition of noise
        noise = np.random.normal(loc=0.0, scale=np.sqrt(self.noise_variance),size=len(conv_seq2))
        
      
        #and lastly the convolution at the receive filter
        filtered_noise = np.convolve(noise, imp_resp_receive, 'Full')
        conv_seq3 = np.convolve(conv_seq2, imp_resp_receive,
                                'Full')/(self.oversampling_fact/self.signaling_factor)
        
        #normalizing the noise
        filtered_noise = filtered_noise/np.sqrt(self.oversampling_fact)
        
        #adding the noise
        conv_seq3 = conv_seq3 + filtered_noise;
       
        #in the non-oversampling case: throw out all unused samples:
        a = conv_seq3.shape[0] % self.oversampling_fact
        
        
        if a != 0:
            conv_seq3 = np.append(conv_seq3, np.zeros((self.oversampling_fact-a)))
    
        conv_seq3 = conv_seq3.reshape(-1,self.oversampling_fact)
        conv_seq3 = conv_seq3[:,0:int(self.T_s/T_r)]
        conv_seq3 = conv_seq3.flatten();
        
        
        self.last_sequence_received = True
        return conv_seq3