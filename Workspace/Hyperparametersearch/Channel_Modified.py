import numpy as np

#This class implements a transmission channel model, which supports pseudo-analog signals
#Intersymbolinterference can be achieved by setting the channels memory
#Moreover faster than Nyquist signaling and receiving is supported 
#Without loss of generality the symbol duration T is always set to equal 1
#memory time delay can also be fractional

class Static_Channel:
	def __init__(self,  memory_length = 2, memory_coeffs = [(1,0),(0.5,1)],
				 signal_filter="sinc", oversampling_factor=5):
		#minimal difference in noise variance at the output is caused by noise being filtered
		#with an ideal low-pass in the non-oversampling case versus an suboptimal low-pass
		#in the case of oversampling
		self.output_overs = oversampling_factor
	
		#snr must be set via set_snr method
		self.snr = None
		
		#checking whether there is the right number of coefficients for the length of the channel
		#each coeffcient is a tuple (amplitude, time delay) of the corresponding tap
		#the direct path (with no delay) is described by memory_coefficients[0]
		assert memory_length == len(memory_coeffs)
		self.memory_length = memory_length
		self.memory_coeffs = memory_coeffs

		#ensure that a sequence can only be received one time
		self.last_sequence_received = True 
		#symbol rate at which the channel output is sampled
		self.T_s = 0

		self.last_sequence_received = True

		#finding the lowest oversampling factor:
		lowest_count = 0
		for i in range(1,101):
			int_arr = (i*np.array([x[1] for x in self.memory_coeffs])).astype("int")
			float_arr = i*np.array([x[1] for x in self.memory_coeffs])
			if np.array_equal(int_arr, float_arr):
				lowest_count = i
				break
		#making sure the oversampling factor is not higher than 100 
		assert lowest_count != 0
		#the lowest oversampling factor for calculation is the least common multiplier of the oversampling factor at the channel output 
		#and the minimum oversampling factor for calculating the impulse responses
		self.oversampling_fact = int(self.lcm(self.output_overs, lowest_count))
		assert self.oversampling_fact <= 100

		#the actual pulse shaping filters are created
		if signal_filter == "sinc":  
			self.signal_filter = self.sinc_filter(self.oversampling_fact)
			self.noise_filter = self.sinc_filter(self.oversampling_fact)
		elif signal_filter == "rc":
			self.signal_filter = self.rc_filter(self.oversampling_fact, roll_off=0.5)
			self.noise_filter = self.rrc_filter(self.oversampling_fact, roll_off=0.5)

		
		#creating the propagation channel impulse response
		delay_array = np.array([x[1] for x in self.memory_coeffs])
		mem_amp_coeffs = np.array([x[0] for x in self.memory_coeffs])
		if delay_array[0] != 0:
			delay_array = np.insert(delay_array,0,0)
			mem_amp_coeffs = np.insert(mem_amp_coeffs,0,0)

		diff_delay_array = (np.diff(delay_array)*self.oversampling_fact).astype("int")
		counter = 0
		i = 0
		if diff_delay_array != []:
			while True:
				if mem_amp_coeffs[i] != 0 or i==0:
					mem_amp_coeffs = np.insert(mem_amp_coeffs,i+1,np.zeros(diff_delay_array[counter]-1))
					counter += 1
				if counter == len(diff_delay_array):
					break
				i += 1

		self.time_corrected_mem_coeffs = mem_amp_coeffs

		#the effective impulse response of the channel
		self.channel_response = np.convolve(self.time_corrected_mem_coeffs, self.signal_filter, mode="full")

		#now the power of the receive signal can be estimated
		self.signal_power = self.calc_signal_power()
	
	def sinc_filter(self, sampling_freq):
		t = np.arange(-10,10+1/sampling_freq,1/sampling_freq)
		
		#Achtung!!!! andere Implementierung von sinc() in Numpy als in Mathematica!
		return np.sinc(t)

	#raised cosine filter for interpolating between taps of the channel response h_c(t)
	def rc_filter(self, sampling_freq, roll_off):
		t = np.linspace(-2,2, num=(4*sampling_freq+1))
		rc = np.piecewise(t, [t==0, np.abs(t) == 1/float(2*roll_off), (t!=0) & (np.abs(t) != 1/float(2*roll_off))], [lambda t:1,  
						  lambda t: np.pi/4*np.sin(np.pi/(2*roll_off))/(np.pi/(2*roll_off)), 
						  lambda t:np.sin(np.pi*t)/(np.pi*t)*np.cos(roll_off*np.pi*t)/(1-(2*roll_off*t)**2)])

		return rc

	#the noise must be convolved with only one filter
	def rrc_filter(self, sampling_freq, roll_off):
		t = np.linspace(-2.4,2.4, num=int(4.8*sampling_freq+1))
		rrc = np.piecewise(t, [t==0, np.abs(t) == 1/float(4*roll_off), (t!=0) & (np.abs(t) != 1/float(4*roll_off))], [
							lambda t:(1-roll_off+4*roll_off/np.pi), 
							lambda t: roll_off/np.sqrt(2)*((1+2/np.pi)*np.sin(np.pi/(4*roll_off))+(1-2/np.pi)*np.cos(np.pi/(4*roll_off))),
							lambda t: (np.sin(np.pi*t*(1-roll_off))+4*roll_off*t*np.cos(np.pi*t*(1+roll_off)))/(np.pi*t*(1-(4*roll_off*t)**2))])
		return rrc


	def calc_signal_power(self):
		test_input = np.random.choice([-1,1],100000)
		test_input = test_input.reshape(-1, len(test_input))
		test_input = np.pad(test_input,[(0,0),(0, self.oversampling_fact-1)],
							   'constant',constant_values=(0))
		test_input = test_input.reshape(-1)
		test_sequence = np.convolve(test_input, self.channel_response, mode="full")
		signal_power = 1/len(test_sequence)*1/self.oversampling_fact*np.sum(test_sequence**2)
		return signal_power

	def set_snr(self, snr):
		self.snr = snr

	#because np.lcm was introduced in numpy version 1.15 and ther hpc version of numpy is 1.14
	#lcm and gcd have to be implemented by us
	def gcd(self, a ,b):
		"""Compute the greatest common divisor of a and b"""
		while b > 0:
			a, b = b, a % b
		return a
	
	def lcm(self, a, b):
		"""Compute the lowest common multiple of a and b"""
		return a * b / self.gcd(a, b)

	
	
	#sequence - sequence to be sent
	#T_s - symbol period
	#signaling_factor - the period of the transmission filter will be T_s/signaling_factor
	
	def transmit(self, sequence):
		assert self.last_sequence_received == True
		assert self.snr != None
		self.sequence = sequence
		self.last_sequence_received = False
		
	def receive(self):
		#make sure that no sequence can be received more than once
		assert self.last_sequence_received == False
		#make sure the channel output is not sampled higher than allowed
		assert self.oversampling_fact >= self.output_overs
		
		
		#the actual distance of the symbols has to be adapted for the convolution according to the chosen
		#sampling frequency
		self.sequence = self.sequence.reshape(len(self.sequence),-1)
		self.sequence = np.pad(self.sequence,[(0,0),(0, self.oversampling_fact-1)],
							   'constant',constant_values=(0))
		self.sequence = self.sequence.reshape(-1)
		
		#convolution of the transmit sequence with the impulse response
		conv_seq = np.convolve(self.channel_response, self.sequence, mode="full")
		
		#get rid of the last couple of elements
		if len(conv_seq) %self.oversampling_fact != 0:
			conv_seq = conv_seq[:-(len(conv_seq)%self.oversampling_fact)]
		
		#then the creation of noise
		#Since th creation of noise with a multivariate normal distribution takes very long for large sequences,
		#two methods for creating the noise are implemented. The other method works via convolving the output of a standard
		#normal distribution with the noise_filter. Since the autocorrelation function starts with a value of one, we
		#throw away the samples in the beginning of the sequence and at the end.
		#To control which of the methods is used there is a flag, called noise_flag, which is set to 1, which causes the use
		#of the second method.
		#the noise is created directly with a certain covariance
		#without without being convolved with a filter function
		
		noise_flag = 1
		#calculating the noise variance according to the current signal strength 
		#with respect to the current propagation channel
		noise_variance = self.signal_power * 10**(-self.snr/10)

		if noise_flag == 0:
			covariance = np.eye(len(conv_seq))
			covariance = np.apply_along_axis(lambda m: np.convolve(m, self.noise_filter), 1, covariance)
			#now throw away everything before the main peak of the first row and everything after the main 
			#peak in the last row
			covariance = covariance[:,int(len(self.noise_filter)/2):]
			covariance = covariance[:,:-int(np.floor(len(self.noise_filter)/2))]
			

			#now the matrix has to be normalized (var(x) = 1*noise_var)
			covariance = covariance/np.amax(covariance)*noise_variance
			#now we can sample the noise vector
			filtered_noise = np.random.multivariate_normal(mean=np.zeros(len(conv_seq)), cov=covariance)
		
		elif noise_flag == 1:
			#to be able to throw away some samples
			noise_length = len(conv_seq)+2*len(self.noise_filter)
			white_noise = np.random.normal(loc=np.zeros((noise_length)), scale=noise_variance, size=noise_length)

			filtered_noise = np.convolve(self.noise_filter, white_noise, mode="full")
			filtered_noise = filtered_noise[len(self.noise_filter):len(self.noise_filter)+len(conv_seq)]




		#finally noise and signal are combined
		conv_seq = np.add(conv_seq, filtered_noise)
		
		#for phase synchronisation the channel output has to be aligned in a way, that the maximum of the impulse
		#response is located in the middle of a vector sampled at the output
		#this is done in the following lines
		start = int(np.argmax(self.channel_response) - int(0.5*self.oversampling_fact))
		stop  = int(start+ len(self.sequence))
		conv_seq = conv_seq[start:stop]

		#downsampling
		#throw away all samples that are not needed for the output
		#to achieve this a "keep-mask" is created
		#make sure the oversampling at the output is possible with the internal representation
		keep_distance = int(self.oversampling_fact/self.output_overs)
		keep_point = np.floor(self.oversampling_fact/2)
		keep_mask = [int(keep_point)]
		
		for i in range(1,int(self.oversampling_fact/2)+2):
			if i*keep_distance+keep_point < self.oversampling_fact:
				keep_mask.append(int(keep_point+i*keep_distance))
			if keep_point - i*keep_distance >= 0:
				keep_mask.append(int(keep_point- i*keep_distance))
		keep_mask = np.sort(keep_mask)
		
		conv_seq = conv_seq.reshape(-1,self.oversampling_fact)
		conv_seq = np.take(conv_seq,keep_mask, axis=1)
		conv_seq = conv_seq.flatten()
		
		self.last_sequence_received = True
		return conv_seq