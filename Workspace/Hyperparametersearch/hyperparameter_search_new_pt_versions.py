# -*- coding: utf-8 -*-

import sys
import os

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pandas as pd

from Channel_Modified import Static_Channel

#slurm issue?
sys.path.append(os.getcwd()) 

torch.set_num_threads(64)

class SBRNN(nn.Module):
	def __init__(self, input_shape, num_lstm1, num_lstm2, num_dense1, num_dense2, training=True):
		super(SBRNN,self).__init__()
		self.inp_shape = input_shape
		self.outp_shape = (input_shape[0], input_shape[1], num_dense2)
		
		self.num_lstm1 = num_lstm1
		self.num_lstm2 = num_lstm2
		self.num_dense1 = num_dense1
		self.num_dense2 = num_dense2
		self.training = training
		self.init_hidden()
		self.last_window = False
		
		if self.training==False:
			#counter for correct averaging
			self.init_counter()
			#memory to store results of past predictions
			self.memory = np.zeros((self.inp_shape[0],self.inp_shape[1],num_dense2))
		
		
		#defining the layers
		#input has shape (sequ_len, batch, num_features)
		self.lstm1 = nn.LSTM(self.inp_shape[2], self.num_lstm1 ,num_layers=1, bidirectional=True)
		self.concat1 = nn.Linear(self.num_lstm1*2, self.num_lstm1)       #*2 bidirectional
		self.lstm2 = nn.LSTM(self.num_lstm1, self.num_lstm2, num_layers=1, bidirectional=True)
		self.concat2 = nn.Linear(self.num_lstm2*2, self.num_lstm2)       #*2 bidirectional
		self.dense1 = nn.Linear(self.num_lstm2, self.num_dense1)
		self.dense2 = nn.Linear(self.num_dense1, self.num_dense2)    
	
	def init_hidden(self, hidden1=None, hidden2=None):
		if hidden1==None and hidden2==None:
			#tuple of 2 tensors because LSTMs have a second state, which must be initialized
			#2 in 1st dimension, because of bidirectional LSTMs
			#                                batch_size            
			self.hidden1 = (torch.randn(2, self.inp_shape[1], self.num_lstm1), 
							torch.randn(2, self.inp_shape[1], self.num_lstm1))
		
			self.hidden2 = (torch.randn(2, self.inp_shape[1], self.num_lstm2), 
							torch.randn(2, self.inp_shape[1], self.num_lstm2))
		#if the hidden state is passed between different windows
		else:
			self.hidden1 = hidden1
			self.hidden2 = hidden2
		
	def init_counter(self):
		self.counter = 1
		self.last_window = False
		
	def forward(self,x):
		x, self.hidden1 = self.lstm1(x, self.hidden1)
		x = F.relu(self.concat1(x))
		x, self.hidden2 = self.lstm2(x, self.hidden2)
		x = F.relu(self.concat2(x))
		x = F.relu(self.dense1(x))
		x = self.dense2(x)
		
		if self.training == True:
			return x
		else:
			x = F.softmax(x, dim=2)
			x = self._average_outputs(x)
		return x
	
	#to be only used without training, gradient is lost
	def _average_outputs(self,x):
		self.memory += x.detach().numpy()

		if self.last_window == False:
			x = torch.from_numpy(self.memory[0,:,:])/float(self.counter)
			
			#reshaping x to always have the same dimensions
			x = x.reshape(-1,self.outp_shape[1], self.outp_shape[2])
		   
			#shift one element to the left and fill with zeros
			self.memory = np.roll(self.memory, -1, axis=0)
			self.memory[-1,:,:] = np.zeros((self.inp_shape[1],num_dense2))
			#to correctly normalize the outputs in the first window
			if self.counter < self.inp_shape[0]:
				self.counter += 1
			
			
		elif self.last_window == True:
			for i in range(self.inp_shape[0]):
				self.memory[i,:,:] = self.memory[i,:,:]/self.counter
				if i>= self.inp_shape[0]-self.counter:
					self.counter -= 1
			x = torch.from_numpy(self.memory)
		
		return x

def binary_target_correction(ybatch):
	y_binary = np.array([[0 if j==-1 else 1 for j in i] for i in ybatch])        
	return y_binary

def target_correction(y_pred_batch):
	shape = y_pred_batch.shape
	y_pred_batch = np.argmax(y_pred_batch, axis=2)    
	y_pred_batch = y_pred_batch.reshape((shape[0],shape[1], 1))
	y_pred_batch[y_pred_batch==0] = -1
	return y_pred_batch

def digitize(sequence, high, low, bits):
	bins = np.linspace(low, high, 2**bits)
	for i in range(len(sequence)):
		bin_index = np.argmin(np.abs(bins-sequence[i]))
		sequence[i] = bins[bin_index]
	sequence = sequence/high
	return sequence


num_dense2 = 2
oversampling_factor = 5
sequ_length = 1000
batch_size = 32
window_size = 40

hc_a = [(0.1,0),(0.25,1),(0.16,2),(0.08,3),(0.04,4)]
input_shape = (window_size, batch_size, oversampling_factor)
best_model_path = "best_model_so_far.pt"

#defining how many tests should be run
hm_tests = 100

test_sequ_length = 10000
test_batch_size = 1
test_input_shape = (window_size, test_batch_size, oversampling_factor)

for test_nr in range(hm_tests):
	#randomly selecting the parameters
	num_lstm1 = int(np.random.choice(np.arange(100,1000)))
	num_lstm2 = int(np.random.choice(np.arange(100,1000)))
	num_dense1 = int(np.random.choice(np.arange(50,500)))
	num_epochs = int(np.random.choice(np.arange(1,50)))
	while True:
		upper = np.random.choice(np.arange(15,40))
		lower = np.random.choice(np.arange(15,40))
		step = np.random.choice([1,0.5,0.2])
		if lower < upper:
			break

	
	channel = Static_Channel(len(hc_a), hc_a, "rc", oversampling_factor)
	
	sbrnn = SBRNN(input_shape, num_lstm1, num_lstm2, num_dense1, num_dense2)
	optimizer = optim.Adam(sbrnn.parameters(),lr=0.005)
	
	print("------ begin training SNR ",upper, " to ", lower," -------")
	
	#the outer training loop (SNRs)
	for snr_db_times in range(int(upper/step),int(lower/step),-1):
		snr_db = snr_db_times*step
		
		channel.set_snr(snr_db)
		progress = (int(upper/step)-snr_db_times)/(int(upper/step)-int(lower/step))
		
		maximum = sum([x[0] for x in hc_a])
		minimum = -maximum
			
	
		#arrays for new sequences (to be windowed)
		x_block = np.zeros((sequ_length,batch_size,1), dtype="int64")
		y_block = np.zeros((sequ_length, batch_size, oversampling_factor))
	
		#filling the arrays
		for i in range(batch_size):
			x_seq = np.random.choice([-1,1],sequ_length,[0.5,0.5])
			x_seq = x_seq.astype("int16")
			channel.transmit(x_seq)
	
			y_seq = channel.receive()
			
			#resolution 10 bits
			if progress < 0.1:
				resolution = 10
				y_seq = digitize(y_seq, maximum, minimum, 10)
			#resolution 6 bits
			elif(progress < 0.2):
				resolution = 6
				y_seq = digitize(y_seq, maximum, minimum, 6)
			#resolution 2 bits
			elif(progress < 0.3):
				resolution = 2
				y_seq = digitize(y_seq, maximum, minimum, 2)
			#resolution 1 bits
			elif(progress >= 0.3):
				resolution = 1
				y_seq = digitize(y_seq, maximum, minimum, 1)
			
	
			#reshaping y to give it its vectorized form for training 
			y_seq = y_seq.reshape(sequ_length, oversampling_factor)
			x_seq = x_seq.reshape(-1,1)
	   
			x_block[:,i,:] = x_seq
			y_block[:,i,:] = y_seq
		
		y_block = y_block.astype("float32")
	
		#using the same sequences for num_epochs
		for epoch in range(num_epochs):
			#iterating over the windows
			num_windows = sequ_length - window_size + 1
			for i in range(num_windows):
				#switching x and y to get from channel notation to machine-learning notation
				batchY = x_block[i:i+window_size,:,:]
				batchX = y_block[i:i+window_size,:,:]
			
				batchY = binary_target_correction(batchY)
	
				#converting to pytorch tensors
				batchX = torch.from_numpy(batchX)
				batchY = torch.from_numpy(batchY)
			
			
				output = sbrnn(batchX)
				output = output.view(-1,2)
				batchY = batchY.view(-1)
				#saving the hidden state for the next window
				hidden1_temp = (sbrnn.hidden1[0].detach(), sbrnn.hidden1[1].detach())
				hidden2_temp = (sbrnn.hidden2[0].detach(), sbrnn.hidden2[1].detach())
			
		   
				loss = nn.CrossEntropyLoss()(output, batchY)
				#emptying the gradient buffers from before
				optimizer.zero_grad()
				sbrnn.init_hidden(hidden1_temp, hidden2_temp)
				loss.backward()
				optimizer.step()
			
			print("Training with SNR = ",snr_db, " epoch ",epoch+1, "von ",num_epochs," Loss = ",
				  loss.detach().numpy(), " resolution = ", resolution)
			sbrnn.init_hidden()
		
	print("--------------- end training SNR ",upper, " to ", lower," -----------\n")
	print("--------------- begin testing  Test Nr.: ",test_nr," -------------\n")
	
	
	num_bit_errors_net = 0
	
	state_dict = sbrnn.state_dict()
	sbrnn = SBRNN(test_input_shape, num_lstm1, num_lstm2, num_dense1, num_dense2,training=False)
	sbrnn.load_state_dict(state_dict)
	sbrnn.eval()
	
	
	
	for snr_db in [15,20,25,30]:
		channel.set_snr(snr_db)
		
		transmit_seq = np.random.choice([-1,1],test_sequ_length)
		
		############################## SBRNN Detection Part  ##################################
	
		#first transmission
		channel.transmit(transmit_seq)
	
		received_seq = channel.receive()
		
		# 1-bit quantization
		received_seq = digitize(received_seq, maximum, minimum, 1)

		#reshaping and type conversion for PyTorch
		received_seq = received_seq.reshape(test_sequ_length, test_batch_size, oversampling_factor)
		transmit_seq = transmit_seq.reshape(-1,test_batch_size,1)
		received_seq = received_seq.astype("float32")
	
		#creating an empty array
		sbrnn_prediction = np.zeros((test_sequ_length, test_batch_size, num_dense2))
	
		num_windows = test_sequ_length - window_size + 1
		#iterating over all windows, giving special treatment to the last window
		for i in range(num_windows-1):
			#prediction for 1 element per window
			sbrnn_prediction[i,:,:] = sbrnn(torch.from_numpy(received_seq[i:i+window_size,:,:]))
		
			hidden1_temp = (sbrnn.hidden1[0].detach(), sbrnn.hidden1[1].detach())
			hidden2_temp = (sbrnn.hidden2[0].detach(), sbrnn.hidden2[1].detach())
			sbrnn.init_hidden(hidden1_temp, hidden2_temp)
		
		#last window results in window_size predictions elements
		sbrnn.last_window = True
		sbrnn_prediction[-window_size:,:,:] = sbrnn(torch.from_numpy(received_seq[-window_size:,:,:]))
		#restart the counter and init the hidden state
		sbrnn.init_counter()
		sbrnn.init_hidden()
	
		#to transform the 0s and 1s to -1s and 1s
		sbrnn_prediction = target_correction(sbrnn_prediction)

		num_bit_errors_net += np.count_nonzero(sbrnn_prediction-transmit_seq)
	
	df = pd.DataFrame({"Upper SNR":[upper], "Lower SNR":[lower], "step size":[step],
					   "Total Biterrors":[num_bit_errors_net], "num_lstm1":[num_lstm1],
					   "num_lstm2": num_lstm2, "num_dense1": num_dense1, "num_epochs":num_epochs},
					   index=[test_nr])
	
	if test_nr == 0:
		best_result = num_bit_errors_net
		torch.save(sbrnn.state_dict(), best_model_path)
		df.to_csv("SNR_Regime_Test_Result.csv")
		
	elif num_bit_errors_net < best_result:
		best_result = num_bit_errors_net
		torch.save(sbrnn.state_dict(), best_model_path)
		with open("SNR_Regime_Test_Result.csv","a") as f:
			df.to_csv(f, header=False)
	else:
		with open("SNR_Regime_Test_Result.csv","a") as f:
			df.to_csv(f, header=False)
   
	print("--------------- end testing SNR ",upper, " to ", lower," ----------\n")