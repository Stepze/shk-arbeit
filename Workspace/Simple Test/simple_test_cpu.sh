#!/bin/bash
#
#SBATCH --job-name=simple_test
#SBATCH --output=out_cpu.txt
#SBATCH --error=errors.txt
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --partition=haswell64
#SBATCH --mem=8000
#SBATCH --time=05:00:00


export OMP_NUM_THREADS=24

module load modenv/scs5
module load PyTorch/0.3.1-foss-2018a-Python-3.6.4-CUDA-9.1.85
module load matplotlib/2.1.2-foss-2018a-Python-3.6.4

python3 simple_test_old_pt_cpu.py
