import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time


start = time.time()
class Simple_net(nn.Module):
    def __init__(self):
        super(Simple_net, self).__init__()
        self.dense1 = nn.Linear(1,100)
        self.dense2 = nn.Linear(100,2000)
        self.dense3 = nn.Linear(2000,1)
    

    def forward(self,x):
        x = F.relu(self.dense1(x))
        x = F.relu(self.dense2(x))
        x = self.dense3(x)

        return x


simple_net = Simple_net()


num_epochs = 20
batch_size = 64
optimizer = optim.Adam(simple_net.parameters(), lr=0.005)
loss_function = nn.MSELoss()
loss_list = [[],[],[],[]]



train_data = np.random.uniform(-20,20,640000)
train_targets = np.square(train_data)

test_data = np.random.uniform(-20,20,64000)
test_targets = np.square(test_data)

cumulative_loss = 0
for epoch in range(num_epochs):
    print("epoch ", epoch+1, " von ", num_epochs)
    for batch_nr in range(int(len(train_data)/batch_size)):
        batchX = train_data[batch_nr*64:(batch_nr+1)*64].astype("float32")
        batchX = batchX.reshape(1,64,1)
        batchX = torch.from_numpy(batchX)

        batchY = train_targets[batch_nr*64:(batch_nr+1)*64].astype("float32")
        batchY = batchY.reshape(1,64,1)
        batchY = torch.from_numpy(batchY)

        nn_pred = simple_net(batchX)
        loss = loss_function(nn_pred, batchY)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        cumulative_loss += loss.detach().numpy()

        if batch_nr % 10 == 0:
            #batch from test set
            test_batchX = test_data[int(batch_nr/10*64):int((batch_nr/10+1)*64)].astype("float32")
            test_batchX = test_batchX.reshape(1,64,1)
            test_batchX = torch.from_numpy(test_batchX)
            test_batchY = test_targets[int(batch_nr/10*64):int((batch_nr/10+1)*64)].astype("float32")
            test_batchY = test_batchY.reshape(1,64,1)
            test_batchY = torch.from_numpy(test_batchY)
            nn_test_pred = simple_net(test_batchX)
            test_loss = loss_function(nn_test_pred, test_batchY)
            test_loss = test_loss.detach().numpy()

            loss_list[0].append(epoch)
            loss_list[1].append(batch_nr)
            loss_list[2].append(cumulative_loss/10)
            loss_list[3].append(test_loss)
            
            cumulative_loss = 0


stop = time.time()
print("------------------------- done with training------------------------------------")
print("training has taken ", stop-start, " seconds")

fig, ax = plt.subplots()
ax.set_title("Verlauf des Losses")
ax.set_ylabel("Loss")
ax.set_xlabel("Training")
ax.set_yscale("log")
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.7)
ax.plot(loss_list[2], label="training loss")
ax.plot(loss_list[3], label="test loss")


plt.legend()
plt.savefig("loss.pdf")