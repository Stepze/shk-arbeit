# -*- coding: utf-8 -*-
import torch
import torch.autograd as autograd
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as tkr
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from Channel_Modified import Static_Channel

import time

class SBRNN(nn.Module):
    def __init__(self, input_shape, num_lstm1, num_dense2, training=True):
        super(SBRNN,self).__init__()
        self.inp_shape = input_shape
        self.outp_shape = (input_shape[0], input_shape[1], num_dense2)
        
        self.num_lstm1 = num_lstm1
        self.num_dense2 = num_dense2
        self.training = training
        self.init_hidden()
        self.last_window = False
        
        if self.training==False:
            #counter for correct averaging
            self.init_counter()
            #memory to store results of past predictions
            self.memory = np.zeros((self.inp_shape[0],self.inp_shape[1],num_dense2))
        
        
        #defining the layers
        #input has shape (sequ_len, batch, num_features)
        self.lstm1 = nn.LSTM(self.inp_shape[2], self.num_lstm1 ,num_layers=1, bidirectional=True)
        self.concat1 = nn.Linear(self.num_lstm1*2, self.num_lstm1)       #*2 bidirectional
        self.dense2 = nn.Linear(self.num_lstm1, self.num_dense2)    
    
    def init_hidden(self, hidden1=None):
        if hidden1==None:
            self.hidden1 = (Variable(torch.randn(2, self.inp_shape[1], self.num_lstm1)).cuda(), 
                            Variable(torch.randn(2, self.inp_shape[1], self.num_lstm1)).cuda())
        
        else:
            self.hidden1 = (Variable(hidden1[0]).cuda(), Variable(hidden1[1]).cuda())
        
    def init_counter(self):
        self.counter = 1
        self.last_window = False
        
    def forward(self,x):
        x, self.hidden1 = self.lstm1(x, self.hidden1)
        x = F.relu(self.concat1(x))
        x = self.dense2(x)
        
        if self.training == True:
            return x
        else:
            x = F.softmax(x, dim=2)
            x = self._average_outputs(x)
        return x
    
    #to be only used without training, gradient is lost
    def _average_outputs(self,x):
        self.memory += x.data.numpy()

        if self.last_window == False:
            x = torch.from_numpy(self.memory[0,:,:])/float(self.counter)
            
            #reshaping x to always have the same dimensions
            x = x.reshape(-1,self.outp_shape[1], self.outp_shape[2])
           
            #shift one element to the left and fill with zeros
            self.memory = np.roll(self.memory, -1, axis=0)
            self.memory[-1,:,:] = np.zeros((self.inp_shape[1],num_dense2))
            #to correctly normalize the outputs in the first window
            if self.counter < self.inp_shape[0]:
                self.counter += 1
            
            
        elif self.last_window == True:
            for i in range(self.inp_shape[0]):
                self.memory[i,:,:] = self.memory[i,:,:]/self.counter
                if i>= self.inp_shape[0]-self.counter:
                    self.counter -= 1
            x = torch.from_numpy(self.memory)
        
        return x

#defining the parameters
save_path = "pytorch_model.pt"
sequ_length = 1000

#defining the hyperparameters
oversampling_factor = 5
batch_size = 32
window_size = 50
num_lstm1 = 1024
num_dense2 = 2
num_epochs = 80


input_shape = (window_size, batch_size, oversampling_factor)

def binary_target_correction(ybatch):
        y_binary = np.array([[0 if j==-1 else 1 for j in i] for i in ybatch])        
        return y_binary
    
def digitize(sequence, high, low, bits):
    bins = np.linspace(low, high, 2**bits)
    for i in range(len(sequence)):
        bin_index = np.argmin(np.abs(bins-sequence[i]))
        sequence[i] = bins[bin_index]
    sequence = sequence/high
    
    return sequence

def numfmt(x, pos): # your custom formatter function: divide by 100.0
    s = '{}'.format(int(x / 10))
    return s


sbrnn = SBRNN(input_shape, num_lstm1, num_dense2)
sbrnn.cuda()
hc_a = [(0.1,0),(0.25,1),(0.16,2),(0.08,3),(0.04,4)]
maximum = sum([x[0] for x in hc_a])
minimum = -maximum
channel = Static_Channel(len(hc_a), hc_a, "rc", oversampling_factor=oversampling_factor)
channel.set_snr(100)
optimizer = optim.Adam(sbrnn.parameters(),lr=0.01)
loss_function = nn.CrossEntropyLoss()
cumulative_loss = 0
loss_list = [[],[],[],[]]



train_data = np.zeros((sequ_length, batch_size, oversampling_factor))
train_targets = np.random.choice([-1,1], (sequ_length, batch_size, 1))
for i in range(batch_size):
    channel.transmit(train_targets[:,i,0])
    train_data[:,i,:] = digitize(channel.receive(),maximum, minimum, 1).reshape(sequ_length,oversampling_factor)
train_targets = binary_target_correction(train_targets)
train_data = train_data.astype("float32")
train_targets = train_targets.astype("int64")
    
test_data = np.zeros((window_size, batch_size, oversampling_factor))
test_targets = np.random.choice([-1,1], (window_size, batch_size, 1))
for i in range(batch_size):
    channel.transmit(test_targets[:,i])
    test_data[:,i,:] = digitize(channel.receive(), maximum, minimum,2).reshape(window_size,oversampling_factor)
test_targets = binary_target_correction(test_targets)
test_data = test_data.astype("float32")
test_targets = test_targets.astype("int64")


start = time.time()
for epoch in range(num_epochs):
    num_windows = sequ_length - window_size + 1
    for i in range(num_windows):
        winY = train_targets[i:i+window_size,:]
        winX = train_data[i:i+window_size,:,:]
        
        winY = Variable(torch.from_numpy(winY)).cuda()
        winX = Variable(torch.from_numpy(winX)).cuda()

        output = sbrnn(winX)
        output = output.view(-1,2)
        winY = winY.view(-1)
        hidden1_temp = (sbrnn.hidden1[0].cpu().data, sbrnn.hidden1[1].cpu().data)
        

        loss = loss_function(output, winY)
        optimizer.zero_grad()
        loss.backward()
        sbrnn.init_hidden(hidden1_temp)
        optimizer.step()
        cumulative_loss += loss.cpu().data.numpy()

        #estimate the performance with one test window every 100 windows
        if i % 100 == 0:
            test_winX = Variable(torch.from_numpy(test_data)).cuda()
            test_winY = Variable(torch.from_numpy(test_targets)).cuda()
            sbrnn.init_hidden()
            test_output = sbrnn(test_winX)
            test_output = test_output.view(-1,2)
            test_winY = test_winY.view(-1)

            test_loss = loss_function(test_output, test_winY).cpu().data.numpy()
            sbrnn.init_hidden(hidden1_temp)
            print("average training loss: ", cumulative_loss/100, 
                  ", loss during test: ", test_loss, ", resolution: ", 1, " bits")
            loss_list[2].append(cumulative_loss/100)
            loss_list[3].append(test_loss)
            loss_list[0].append(epoch)
            cumulative_loss = 0
            
stop = time.time()
print("Time taken for training: ", stop-start, " seconds")

xfmt = tkr.FuncFormatter(numfmt)
fig, ax = plt.subplots(figsize=(12,12))
ax.set_title("Verlauf des Losses")
ax.set_ylabel("Loss")
ax.set_xlabel("Training Epochs")
ax.xaxis.set_major_locator(MultipleLocator(10))
ax.tick_params(axis='x', which='major', bottom=False)
ax.xaxis.set_major_formatter(xfmt)

ax.set_yscale("log")
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.7)
ax.plot(loss_list[2], label="training loss")
ax.plot(loss_list[3], label="test loss")
plt.legend()
plt.savefig("overfitting_test_2.pdf")


                

df = pd.DataFrame({"epoch": loss_list[0],
                   "training_loss": loss_list[2],
                   "test_loss": loss_list[3]})
df.to_csv("overfitting_test_2.csv")