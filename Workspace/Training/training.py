# -*- coding: utf-8 -*-

import torch
import torch.autograd as autograd
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time

from Channel_Modified import Static_Channel

class SBRNN(nn.Module):
    def __init__(self, input_shape, num_lstm1, num_lstm2, num_dense1, num_dense2, training=True):
        super(SBRNN,self).__init__()
        self.inp_shape = input_shape
        self.outp_shape = (input_shape[0], input_shape[1], num_dense2)
        
        self.num_lstm1 = num_lstm1
        self.num_lstm2 = num_lstm2
        self.num_dense1 = num_dense1
        self.num_dense2 = num_dense2
        self.training = training
        self.init_hidden()
        self.last_window = False
        
        if self.training==False:
            #counter for correct averaging
            self.init_counter()
            #memory to store results of past predictions
            self.memory = np.zeros((self.inp_shape[0],self.inp_shape[1],num_dense2))
        
        
        #defining the layers
        #input has shape (sequ_len, batch, num_features)
        self.lstm1 = nn.LSTM(self.inp_shape[2], self.num_lstm1 ,num_layers=1, bidirectional=True)
        self.concat1 = nn.Linear(self.num_lstm1*2, self.num_lstm1)       #*2 bidirectional
        self.lstm2 = nn.LSTM(self.num_lstm1, self.num_lstm2, num_layers=1, bidirectional=True)
        self.concat2 = nn.Linear(self.num_lstm2*2, self.num_lstm2)       #*2 bidirectional
        self.dense1 = nn.Linear(self.num_lstm2, self.num_dense1)
        self.dense2 = nn.Linear(self.num_dense1, self.num_dense2)    
    
    def init_hidden(self, hidden1=None, hidden2=None):
        if hidden1==None and hidden2==None:
            #tuple of 2 tensors because LSTMs have a second state, which must be initialized
            #2 in 1st dimension, because of bidirectional LSTMs
            #                                batch_size            
            self.hidden1 = (Variable(torch.randn(2, self.inp_shape[1], self.num_lstm1)), 
                            Variable(torch.randn(2, self.inp_shape[1], self.num_lstm1)))
        
            self.hidden2 = (Variable(torch.randn(2, self.inp_shape[1], self.num_lstm2)), 
                            Variable(torch.randn(2, self.inp_shape[1], self.num_lstm2)))
        #if the hidden state is passed between different windows
        else:
            self.hidden1 = hidden1
            self.hidden2 = hidden2
        
    def init_counter(self):
        self.counter = 1
        self.last_window = False
        
    def forward(self,x):
        x, self.hidden1 = self.lstm1(x, self.hidden1)
        x = F.relu(self.concat1(x))
        x, self.hidden2 = self.lstm2(x, self.hidden2)
        x = F.relu(self.concat2(x))
        x = F.relu(self.dense1(x))
        x = self.dense2(x)
        
        if self.training == True:
            return x
        else:
            x = F.softmax(x, dim=2)
            x = self._average_outputs(x)
        return x
    
    #to be only used without training, gradient is lost
    def _average_outputs(self,x):
        self.memory += x.data.numpy()

        if self.last_window == False:
            x = torch.from_numpy(self.memory[0,:,:])/float(self.counter)
            
            #reshaping x to always have the same dimensions
            x = x.reshape(-1,self.outp_shape[1], self.outp_shape[2])
           
            #shift one element to the left and fill with zeros
            self.memory = np.roll(self.memory, -1, axis=0)
            self.memory[-1,:,:] = np.zeros((self.inp_shape[1],num_dense2))
            #to correctly normalize the outputs in the first window
            if self.counter < self.inp_shape[0]:
                self.counter += 1
            
            
        elif self.last_window == True:
            for i in range(self.inp_shape[0]):
                self.memory[i,:,:] = self.memory[i,:,:]/self.counter
                if i>= self.inp_shape[0]-self.counter:
                    self.counter -= 1
            x = torch.from_numpy(self.memory)
        
        return x

def binary_target_correction(ybatch):
        y_binary = np.array([[0 if j==-1 else 1 for j in i] for i in ybatch])
        return y_binary
    
    
def target_correction(y_pred_batch):
    shape = y_pred_batch.shape
    y_pred_batch = np.argmax(y_pred_batch, axis=2)    
    y_pred_batch = y_pred_batch.reshape((shape[0],shape[1], 1))
    y_pred_batch[y_pred_batch==0] = -1
    return y_pred_batch

def digitize(sequence, high, low, bits):
    bins = np.linspace(low, high, 2**bits)
    for i in range(len(sequence)):
        bin_index = np.argmin(np.abs(bins-sequence[i]))
        sequence[i] = bins[bin_index]
    sequence = sequence/high
    
    return sequence

num_lstm1 = 200
num_lstm2 = 150
num_dense1 = 100
num_dense2 = 2
oversampling_factor = 5
sequ_length = 10000
batch_size = 32
window_size = 40
num_epochs = 4
hc_a = [(0.1,0),(0.25,1),(0.16,2),(0.08,3),(0.04,4)]
maximum = sum([x[0] for x in hc_a])
minimum = -maximum
snr_values = [i/10 for i in range(300,240,-10)]

input_shape = (window_size, batch_size, oversampling_factor)
sbrnn = SBRNN(input_shape, num_lstm1, num_lstm2, num_dense1, num_dense2, training=True)
sbrnn.cuda()
channel = Static_Channel(len(hc_a), hc_a, "rc", oversampling_factor)


loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(sbrnn.parameters(), lr=0.005)
cumulative_loss = 0
loss_list = [[],[],[],[]]

print("----------------------------creating training set---------------------------------------")
setX = np.zeros((len(snr_values), sequ_length, batch_size, oversampling_factor))
setY = np.random.choice([-1,1],(len(snr_values), sequ_length, batch_size, 1))

for snr_index in range(len(snr_values)):
        channel.set_snr(snr_values[snr_index])
        
        for i in range(batch_size):
            channel.transmit(setY[snr_index,:,i,0])
            setX[snr_index,:,i,:] = channel.receive().reshape(sequ_length, oversampling_factor)
            
        setY[snr_index,:,:,:] = binary_target_correction(setY[snr_index,:,:,:]).reshape(sequ_length,batch_size,1)

        
print("----------------------------creating test set---------------------------------------")
test_setX = np.zeros((len(snr_values), window_size, batch_size, oversampling_factor))
test_setY = np.random.choice([-1,1],(len(snr_values), window_size, batch_size, 1))

for snr_index in range(len(snr_values)):
        channel.set_snr(snr_values[snr_index])
        
        for i in range(batch_size):
            channel.transmit(test_setY[snr_index,:,i,0])
            test_setX[snr_index,:,i,:] = channel.receive().reshape(window_size, oversampling_factor)
            
        test_setY[snr_index,:,:,:] = binary_target_correction(test_setY[snr_index,:,:,:]).reshape(window_size, 
                                                                                       batch_size,1)
       
start = time.time()
print("-----------------------------begin training--------------------------------------------")
for snr in range(len(snr_values)):
    for epoch in range(num_epochs):
        print("epoch ", epoch, " of ", num_epochs, " with SNR ", snr_values[snr])
        if snr == 0:
            if epoch == 1:
                resolution = 6

            elif epoch == 2:
                resolution = 4

            elif epoch == 3:
                resolution = 2

            elif epoch >= 4:
                resolution = 1
        else:
            resolution = 1
        batchX = setX[snr,:,:,:].astype("float32")
        batchY = setY[snr,:,:,:].astype("int64")

        #quantization
        for i in range(batch_size):
                seq = setX[snr,:,i,:]
                seq = seq.flatten()
                batchX[:,i,:] = digitize(seq, maximum, minimum, resolution).reshape(sequ_length, 
                                                                                   oversampling_factor)
        batchX = Variable(torch.from_numpy(batchX)).cuda()
        batchY = Variable(torch.from_numpy(batchY)).cuda()

        num_windows = sequ_length - window_size + 1
        for i in range(num_windows):
            winY = batchY[i:i+window_size,:,:]
            winX = batchX[i:i+window_size,:,:]

            output = sbrnn(winX)
            output = output.view(-1,2)
            winY = winY.view(-1)

            loss = loss_function(output, winY)
            optimizer.zero_grad()
            sbrnn.init_hidden()
            loss.backward()
            optimizer.step()
            cumulative_loss += loss.cpu().data.numpy()

            #estimate the performance with one test window every 100 windows
            if i % 100 == 0:
                test_winX = test_setX[snr,:,:,:].astype("float32")
                test_winY = test_setY[snr,:,:,:].astype("int64")

                for i in range(batch_size):
                    seq = test_setX[snr,:,i,:]
                    seq = seq.flatten()
                    test_winX[:,i,:] = digitize(seq, maximum, minimum, resolution).reshape(window_size, 
                                                                              oversampling_factor)
                test_winX = Variable(torch.from_numpy(test_winX)).cuda()
                test_winY = Variable(torch.from_numpy(test_winY)).cuda()

                test_output = sbrnn(test_winX)
                test_output = test_output.view(-1,2)
                test_winY = test_winY.view(-1)

                test_loss = loss_function(test_output, test_winY).cpu().data.numpy()
                sbrnn.init_hidden()

                print("snr: ",snr_values[snr], ", average training loss: ", cumulative_loss/100, 
                      ", loss during test: ", test_loss, ", resolution: ", resolution, " bits")
                loss_list[0].append(epoch)
                loss_list[1].append(snr)
                loss_list[2].append(cumulative_loss/100)
                loss_list[3].append(test_loss)
                cumulative_loss = 0
stop = time.time()
print("----------------------------------- done with training --------------------------------")
print("time taken: ", stop-start)
            
df = pd.DataFrame({"epoch": loss_list[0],
                   "snr": loss_list[1],
                   "training_loss": loss_list[2],
                   "test_loss": loss_list[3]})
df.to_csv("training_results.csv")
                
fig, ax = plt.subplots()
ax.set_title("Verlauf des Losses")
ax.set_ylabel("Loss")
ax.set_xlabel("Training")
ax.set_yscale("log")
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.7)
ax.plot(loss_list[2], label="training loss")
ax.plot(loss_list[3], label="test loss")

plt.legend()
plt.savefig("loss.pdf")