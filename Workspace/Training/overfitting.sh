#!/bin/bash
#
#SBATCH --job-name=overfitting_test
#SBATCH --gres=gpu:1
#SBATCH --output=out_overfitting.txt
#SBATCH --error=errors_overfitting.txt
#SBATCH --partition=gpu2
#SBATCH --mem=8000
#SBATCH --time=10:00:00

module load modenv/scs5
module load PyTorch/0.3.1-foss-2018a-Python-3.6.4-CUDA-9.1.85
module load matplotlib/2.1.2-foss-2018a-Python-3.6.4

python3 Overfitting_Test.py