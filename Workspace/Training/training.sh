#!/bin/bash
#
#SBATCH --job-name=training_sbrnn
#SBATCH --gres=gpu:1
#SBATCH --output=out_cuda.txt
#SBATCH --error=errors.txt
#SBATCH --partition=gpu2
#SBATCH --mem=8000
#SBATCH --time=12:00:00
#SBATCH --mail-type=END

module load modenv/scs5
module load PyTorch/0.3.1-foss-2018a-Python-3.6.4-CUDA-9.1.85
module load matplotlib/2.1.2-foss-2018a-Python-3.6.4

python3 training.py