#!/bin/bash
#
#SBATCH --job-name=sbrnn_search
#SBATCH --output=out.txt
#SBATCH --error=errors.txt
#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --partition=haswell
#SBATCH --cpus-per-task=32
#SBATCH --mem=8000
#SBATCH --time=24:00:00

module load modenv/scs5
module load python3.6
module load PyTorch/0.3.1-foss-2018a-Python-3.6.4-CUDA-9.1.85

python3.6 hyperparameter_search.py
